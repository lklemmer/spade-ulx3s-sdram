#
# This file is heavily inspired by gen.py of LiteDRAM.
#
# Original copyright
#
# Copyright (c) 2018-2021 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2020 Stefan Schrijvers <ximin@ximinity.net>
# SPDX-License-Identifier: BSD-2-Clause

from typing import Any, Tuple, Dict
import yaml
import argparse
import pathlib
import os

class Port:
    def __init__(
        self,
        verilog_head,
        verilog_body,
        # The port defintiion of the #[no_mangle] entity to be generated
        spade_stub_head,
        # Extra code in the body of the spade module to map ports to
        # spade struct
        spade_body,
        # Bindings from the things defined in spade_body to the stub
        spade_bindings,
        # Port binding to return from the module
        spade_return_port,
        # Output port definitions of the spade module
        spade_output_type,
    ) -> None:
        self.verilog_head = verilog_head
        self.verilog_body = verilog_body
        self.spade_stub_head = spade_stub_head
        self.spade_body = spade_body
        self.spade_bindings = spade_bindings
        self.spade_output_type = spade_output_type
        self.spade_return_port = spade_return_port

def generate_wishbone(port: Tuple[str, Dict[str, Any]]) -> Port:
    (port_name, props) = port
    verilog_head = "\n    ".join([
        f"input  wire   [23:0] user_port_{port_name}_adr,",
        f"input  wire   [15:0] user_port_{port_name}_dat_w,",
        f"output wire   [15:0] user_port_{port_name}_dat_r,",
        f"input  wire    [1:0] user_port_{port_name}_sel,",
        f"input  wire          user_port_{port_name}_cyc,",
        f"input  wire          user_port_{port_name}_stb,",
        f"output wire          user_port_{port_name}_ack,",
        f"input  wire          user_port_{port_name}_we,",
        f"output wire          user_port_{port_name}_err,",
    ])
    verilog_body = "\n        ".join([
        f", .user_port_{port_name}_adr(user_port_{port_name}_adr)",
        f", .user_port_{port_name}_dat_w(user_port_{port_name}_dat_w)",
        f", .user_port_{port_name}_dat_r(user_port_{port_name}_dat_r)",
        f", .user_port_{port_name}_sel(user_port_{port_name}_sel)",
        f", .user_port_{port_name}_cyc(user_port_{port_name}_cyc)",
        f", .user_port_{port_name}_stb(user_port_{port_name}_stb)",
        f", .user_port_{port_name}_ack(user_port_{port_name}_ack)",
        f", .user_port_{port_name}_we(user_port_{port_name}_we)",
        f", .user_port_{port_name}_err(user_port_{port_name}_err)",
    ])
    spade_stub_head = "\n    ".join([
        f"user_port_{port_name}_adr: int<24>,",
        f"user_port_{port_name}_dat_w: int<16>,",
        f"user_port_{port_name}_dat_r: &mut int<16>,",
        f"user_port_{port_name}_sel: int<2>,",
        f"user_port_{port_name}_cyc: bool,",
        f"user_port_{port_name}_stb: bool,",
        f"user_port_{port_name}_ack: &mut bool,",
        f"user_port_{port_name}_we: bool,",
        f"user_port_{port_name}_err: &mut bool,",
    ])
    spade_body = "\n    ".join([
        f"let ({port_name}, {port_name}_inv) = port;",

        f"let ({port_name}_we, {port_name}_data_w) = match *{port_name}_inv.write {{",
            f"Some(data) => (true, data),",
            f"None => (false, 0) // NOTE: Placeholder 0",
        f"}};"
    ])
    spade_bindings = "\n        ".join([
        f"user_port_{port_name}_adr: *{port_name}_inv.addr,",
        f"user_port_{port_name}_dat_w: {port_name}_data_w,",
        f"user_port_{port_name}_dat_r: {port_name}_inv.data_out,",
        f"user_port_{port_name}_sel: *{port_name}_inv.sel,",
        f"user_port_{port_name}_cyc: *{port_name}_inv.cyc,",
        f"user_port_{port_name}_stb: *{port_name}_inv.stb,",
        f"user_port_{port_name}_ack: {port_name}_inv.ack,",
        f"user_port_{port_name}_we: {port_name}_we,",
        f"user_port_{port_name}_err: {port_name}_inv.err,",
    ])
    spade_return_port = f"{port_name}"

    return Port(
        verilog_head=verilog_head,
        verilog_body=verilog_body,
        spade_stub_head=spade_stub_head,
        spade_body=spade_body,
        spade_bindings=spade_bindings,
        spade_return_port=spade_return_port,
        spade_output_type="DramWishbone"
    )

def generate_axi(port: Tuple[str, Dict[str, Any]]) -> Port:
    (port_name, props) = port
    id_width = props["id_width"]
    verilog_head = "\n    ".join([
        f"input  wire          user_port_{port_name}_awvalid,",
        f"output wire          user_port_{port_name}_awready,",
        f"input  wire   [24:0] user_port_{port_name}_awaddr,",
        f"input  wire    [1:0] user_port_{port_name}_awburst,",
        f"input  wire    [7:0] user_port_{port_name}_awlen,",
        f"input  wire    [3:0] user_port_{port_name}_awsize,",
        f"input  wire    [{id_width-1}:0] user_port_{port_name}_awid,",
        f"input  wire          user_port_{port_name}_wvalid,",
        f"output wire          user_port_{port_name}_wready,",
        f"input  wire          user_port_{port_name}_wlast,",
        f"input  wire    [1:0] user_port_{port_name}_wstrb,",
        f"input  wire   [15:0] user_port_{port_name}_wdata,",
        f"output wire          user_port_{port_name}_bvalid,",
        f"input  wire          user_port_{port_name}_bready,",
        f"output wire    [1:0] user_port_{port_name}_bresp,",
        f"output wire    [{id_width-1}:0] user_port_{port_name}_bid,",
        f"input  wire          user_port_{port_name}_arvalid,",
        f"output wire          user_port_{port_name}_arready,",
        f"input  wire   [24:0] user_port_{port_name}_araddr,",
        f"input  wire    [1:0] user_port_{port_name}_arburst,",
        f"input  wire    [7:0] user_port_{port_name}_arlen,",
        f"input  wire    [3:0] user_port_{port_name}_arsize,",
        f"input  wire    [{id_width-1}:0] user_port_{port_name}_arid,",
        f"output wire          user_port_{port_name}_rvalid,",
        f"input  wire          user_port_{port_name}_rready,",
        f"output wire          user_port_{port_name}_rlast,",
        f"output wire    [1:0] user_port_{port_name}_rresp,",
        f"output wire   [15:0] user_port_{port_name}_rdata,",
        f"output wire    [{id_width-1}:0] user_port_{port_name}_rid",
    ])
    verilog_body = "\n        ".join([
        f", .user_port_{port_name}_awvalid(user_port_{port_name}_awvalid)",
        f", .user_port_{port_name}_awready(user_port_{port_name}_awready)",
        f", .user_port_{port_name}_awaddr(user_port_{port_name}_awaddr)",
        f", .user_port_{port_name}_awburst(user_port_{port_name}_awburst)",
        f", .user_port_{port_name}_awlen(user_port_{port_name}_awlen)",
        f", .user_port_{port_name}_awsize(user_port_{port_name}_awsize)",
        f", .user_port_{port_name}_awid(user_port_{port_name}_awid)",
        f", .user_port_{port_name}_wvalid(user_port_{port_name}_wvalid)",
        f", .user_port_{port_name}_wready(user_port_{port_name}_wready)",
        f", .user_port_{port_name}_wlast(user_port_{port_name}_wlast)",
        f", .user_port_{port_name}_wstrb(user_port_{port_name}_wstrb)",
        f", .user_port_{port_name}_wdata(user_port_{port_name}_wdata)",
        f", .user_port_{port_name}_bvalid(user_port_{port_name}_bvalid)",
        f", .user_port_{port_name}_bready(user_port_{port_name}_bready)",
        f", .user_port_{port_name}_bresp(user_port_{port_name}_bresp)",
        f", .user_port_{port_name}_bid(user_port_{port_name}_bid)",
        f", .user_port_{port_name}_arvalid(user_port_{port_name}_arvalid)",
        f", .user_port_{port_name}_arready(user_port_{port_name}_arready)",
        f", .user_port_{port_name}_araddr(user_port_{port_name}_araddr)",
        f", .user_port_{port_name}_arburst(user_port_{port_name}_arburst)",
        f", .user_port_{port_name}_arlen(user_port_{port_name}_arlen)",
        f", .user_port_{port_name}_arsize(user_port_{port_name}_arsize)",
        f", .user_port_{port_name}_arid(user_port_{port_name}_arid)",
        f", .user_port_{port_name}_rvalid(user_port_{port_name}_rvalid)",
        f", .user_port_{port_name}_rready(user_port_{port_name}_rready)",
        f", .user_port_{port_name}_rlast(user_port_{port_name}_rlast)",
        f", .user_port_{port_name}_rresp(user_port_{port_name}_rresp)",
        f", .user_port_{port_name}_rdata(user_port_{port_name}_rdata)",
        f", .user_port_{port_name}_ri(user_port_{port_name}_ri)",
    ])
    spade_stub_head = "\n    ".join([
        f"user_port_{port_name}_awvalid: bool,",
        f"user_port_{port_name}_awready: &mut bool,",
        f"user_port_{port_name}_awaddr: int<25>,",
        f"user_port_{port_name}_awburst: int<2>,",
        f"user_port_{port_name}_awlen: int<8>,",
        f"user_port_{port_name}_awsize: int<4>,",
        f"user_port_{port_name}_awid: int<{id_width}>,",
        f"user_port_{port_name}_wvalid: bool,",
        f"user_port_{port_name}_wready: &mut bool,",
        f"user_port_{port_name}_wlast: bool,",
        f"user_port_{port_name}_wstrb: int<2>,",
        f"user_port_{port_name}_wdata: int<16>,",
        f"user_port_{port_name}_bvalid: &mut bool,",
        f"user_port_{port_name}_bready: bool,",
        f"user_port_{port_name}_bresp: &mut int<2>,",
        f"user_port_{port_name}_bid: &mut int<{id_width}>,",
        f"user_port_{port_name}_arvalid: bool,",
        f"user_port_{port_name}_arready: &mut bool,",
        f"user_port_{port_name}_araddr: int<25>,",
        f"user_port_{port_name}_arburst: int<2>,",
        f"user_port_{port_name}_arlen: int<8>,",
        f"user_port_{port_name}_arsize: int<4>,",
        f"user_port_{port_name}_arid: int<{id_width}>,",
        f"user_port_{port_name}_rvalid: &mut bool,",
        f"user_port_{port_name}_rready: bool,",
        f"user_port_{port_name}_rlast: &mut bool,",
        f"user_port_{port_name}_rresp: &mut int<2>,",
        f"user_port_{port_name}_rdata: &mut int<16>,",
        f"user_port_{port_name}_rid: &mut int<{id_width}>,",
    ])
    spade_return_port = f"{port_name}"

    spade_body = "\n    ".join([
        f"let ({port_name}, {port_name}_inv) = port;"
    ])
    spade_bindings = "\n        ".join([
        f"user_port_{port_name}_awvalid: *{port_name}_inv.aw.valid,",
        f"user_port_{port_name}_awready: {port_name}_inv.aw.ready,",
        f"user_port_{port_name}_awaddr: *{port_name}_inv.aw.addr,",
        f"user_port_{port_name}_awburst: *{port_name}_inv.aw.burst,",
        f"user_port_{port_name}_awlen: *{port_name}_inv.aw.len,",
        f"user_port_{port_name}_awsize: *{port_name}_inv.aw.size,",
        f"user_port_{port_name}_awid: *{port_name}_inv.aw.id,",

        f"user_port_{port_name}_wvalid: *{port_name}_inv.w.valid,",
        f"user_port_{port_name}_wready: {port_name}_inv.w.ready,",
        f"user_port_{port_name}_wlast: *{port_name}_inv.w.last,",
        f"user_port_{port_name}_wstrb: *{port_name}_inv.w.strb,",
        f"user_port_{port_name}_wdata: *{port_name}_inv.w.data,",

        f"user_port_{port_name}_bvalid: {port_name}_inv.b.valid,",
        f"user_port_{port_name}_bready: *{port_name}_inv.b.ready,",
        f"user_port_{port_name}_bresp: {port_name}_inv.b.resp,",
        f"user_port_{port_name}_bid: {port_name}_inv.b.id,",

        f"user_port_{port_name}_arvalid: *{port_name}_inv.ar.valid,",
        f"user_port_{port_name}_arready: {port_name}_inv.ar.ready,",
        f"user_port_{port_name}_araddr: *{port_name}_inv.ar.addr,",
        f"user_port_{port_name}_arburst: *{port_name}_inv.ar.burst,",
        f"user_port_{port_name}_arlen: *{port_name}_inv.ar.len,",
        f"user_port_{port_name}_arsize: *{port_name}_inv.ar.size,",
        f"user_port_{port_name}_arid: *{port_name}_inv.ar.id,",

        f"user_port_{port_name}_rvalid: {port_name}_inv.r.valid,",
        f"user_port_{port_name}_rready: *{port_name}_inv.r.ready,",
        f"user_port_{port_name}_rlast: {port_name}_inv.r.last,",
        f"user_port_{port_name}_rresp: {port_name}_inv.r.resp,",
        f"user_port_{port_name}_rdata: {port_name}_inv.r.data,",
        f"user_port_{port_name}_rid: {port_name}_inv.r.id,",
    ])
    return Port(
        verilog_head=verilog_head,
        verilog_body=verilog_body,
        spade_stub_head=spade_stub_head,
        spade_body=spade_body,
        spade_bindings=spade_bindings,
        spade_return_port=spade_return_port,
        spade_output_type=f"AxiPort<{id_width}>"
    )

def generate_port(port: Tuple[str, Dict[str, Any]]) -> Port:
    (_, props) = port
    match props["type"]:
        case "wishbone":
            return generate_wishbone(port)
        case "axi":
            return generate_axi(port)
        case other:
            raise ValueError(f"Port of type {other} is unsupported")

def generate_files(
    config: Dict[str, Dict[str, Any]],
    verilog_stub_file: pathlib.Path,
    spade_stub_file: pathlib.Path
) -> Tuple[str, str]:
    ports = [generate_port(port) for port in config["user_ports"].items()]

    with open(verilog_stub_file, 'r') as f:
        verilog_stub = f.read()
    with open(spade_stub_file, 'r') as f:
        spade_stub = f.read()

    verilog_head = "\n".join([port.verilog_head for port in ports])
    verilog_io_mappings = "\n".join([port.verilog_body for port in ports])

    spade_types = ", ".join([port.spade_output_type for port in ports])
    spade_body = "\n".join([port.spade_body for port in ports])
    spade_bindings = "\n".join([port.spade_bindings for port in ports])
    spade_stub_head = "\n".join([port.spade_stub_head for port in ports])
    spade_return_port = ", ".join([port.spade_return_port for port in ports])

    

    return (
        verilog_stub
            .replace("##IO_MAPPINGS##", verilog_io_mappings)
            .replace("##IO_WIRES##", verilog_head),
        spade_stub
            .replace("##TYPES##", spade_types)
            .replace("##BODY##", spade_body)
            .replace("##BINDINGS##", spade_bindings)
            .replace("##WRAPPER_HEAD##", spade_stub_head)
            .replace("##RETURN_PORTS##", spade_return_port)
    )


def main():
    parser = argparse.ArgumentParser(description="LiteDRAM standalone core generator")
    parser.add_argument("--verilog_outdir")
    parser.add_argument("--spade_outdir")
    parser.add_argument("config", help="YAML config file")
    args = parser.parse_args()


    self_dir = pathlib.Path(__file__).parent.resolve()

    config_file = pathlib.Path(args.config)
    verilog_stub = self_dir / "stubs" / "verilog_wrapper.sv"
    spade_stub = self_dir / "stubs" / "spade_stub.spade"

    spade_stub.stat().st_mtime

    out_verilog = pathlib.Path(args.verilog_outdir) / "sdram_wrapper.sv"
    out_spade = pathlib.Path(args.spade_outdir) / "sdram_controller.spade"

    # Check if any files need rebuilding
    targets_exists =  out_verilog.exists() and out_spade.exists()
    if targets_exists:
        oldest_target = min(out_verilog.stat().st_mtime, out_spade.stat().st_mtime)

    needs_rebuild = (not targets_exists) or any(
            map(
                lambda f: not f.exists() or f.stat().st_mtime > oldest_target,
                [config_file, verilog_stub, spade_stub, pathlib.Path(__file__)]
            )
        )

    if not needs_rebuild:
        print("Sdram controller is up to date")
        return
    print("Building sddram controller")

    config = yaml.load(open(args.config).read(), Loader=yaml.Loader)

    (verilog, spade) = generate_files(config, verilog_stub, spade_stub)

    if not os.path.exists(args.spade_outdir):
        os.mkdir(pathlib.Path(args.spade_outdir))

    with open(out_verilog, 'w') as f:
        f.write(verilog)
    with open(out_spade, 'w') as f:
        f.write(spade)

if __name__ == "__main__":
    main()
